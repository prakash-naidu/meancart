(function () {
    'use strict';

    angular
        .module('app.controllers')
        .controller('ConfirmationModalCtrl', ConfirmationModalCtrl);


    /* @ngInject */
    function ConfirmationModalCtrl($scope,
                                   passed_data,
                                   $uibModalInstance) {

        var vm = this;
        vm.message = passed_data.message;
        vm.title = passed_data.title;
        vm.show_cancel=false;
        vm.confirmSelection = confirmSelection;
        vm.rejectSelection = rejectSelection;
        vm.cancel = cancel;
        activate();

        function activate(){
            if( angular.isDefined(passed_data.yes_text) ){
                vm.yes_text=passed_data.yes_text;
            }else{
                vm.yes_text="YES";
            }
            if( angular.isDefined(passed_data.no_text) ){
                vm.no_text=passed_data.no_text;
            }else{
                vm.no_text="NO";
            }
            if( angular.isDefined(passed_data.cancel_text) ){
                vm.show_cancel=true;
                vm.cancel_text=passed_data.cancel_text;
            }
        }

        function confirmSelection() {
            $uibModalInstance.close({
                status: 'yes'
            });
        }
        function rejectSelection() {
            $uibModalInstance.close({
                status: 'no'
            });
        }

        function cancel() {
            $uibModalInstance.dismiss({
                status: 'cancel'
            });
        }

    }

})();


