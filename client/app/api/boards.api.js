(function () {
    "use strict";

    angular.module("app.api").service('BoardsAPI', BoardsAPI);

    /* @ngInject */
    function BoardsAPI($http, SITE_CONSTANTS) {
        var api_route = SITE_CONSTANTS.api_route;
        var mock_api_route = SITE_CONSTANTS.mock_api_route;

        return {
            list: function (callback, errorCatch) {
                $http({
                    method: 'GET',
                    url: api_route + 'list-board'
                }).then(callback, errorCatch);
            },
            create: function (data, callback, errorCatch) {
                $http({
                    method: 'POST',
                    url: api_route + 'admin/board/add',
                    data: data
                }).then(callback, errorCatch);
            },
            update: function (data, callback, errorCatch) {
                $http({
                    method: 'POST',
                    url: api_route + 'admin/board/update',
                    data: data
                }).then(callback, errorCatch);
            },
            delete: function (data, callback, errorCatch) {
                $http({
                    method: 'POST',
                    url: api_route + 'admin/board/delete',
                    data: data
                }).then(callback, errorCatch);
            },
        };
    }
})();

