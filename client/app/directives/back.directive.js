/**
 * Created by roshansachan on 15/01/17.
 */
(function () {
    "use strict";

    angular.module('app.directives').directive('back', backDirective);

    /* @ngInject */
    function backDirective($window) {
        return {
            restrict: 'A',
            link: link,
        };

        function link(scope, element, attrs) {
            element.bind('click', function () {
                $window.history.back();
            });
        }

    }

})();