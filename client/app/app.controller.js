(function () {
    angular.module('app.controllers')
        .controller('AppCtrl', AppCtrl);

    /* @ngInject */
    function AppCtrl($rootScope, $window) {

        if ($window.innerWidth < 991) {
            $rootScope.hlkey_use_paceholder = true;
        } else {
            $rootScope.hlkey_use_paceholder = false;
        }

    }

})();