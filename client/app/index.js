(function () {
    "use strict";

    angular.module('app',
        [
            'app.constants',
            'app.run',
            'app.routes',
            'app.services',
            'app.controllers',
            'app.directives',
            'app.filters',
            'app.factories',
            'app.config',
            'app.api',
            
            'ui.bootstrap',
            'ngSanitize',
            'ngProgress',

        ]);

    angular.module('app.constants', []);
    angular.module('app.run', []);
    angular.module('app.routes', ['ui.router', 'permission', 'permission.ui']);
    angular.module('app.controllers', []);
    angular.module('app.directives', []);
    angular.module('app.filters', []);
    angular.module('app.factories', []);
    angular.module('app.services', []);
    angular.module('app.config', []);
    angular.module('app.api', []);


})();