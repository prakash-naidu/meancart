
(function () {
    'use strict';

    angular
        .module('app.services')
        .service('LogoutService', LogoutService);

    /* @ngInject */
    function LogoutService($window, $state) {
        this.logout = logout;
        function logout() {
            var local_stogare_item = ['token'];
            angular.forEach(local_stogare_item, function (value, key) {
                $window.localStorage.removeItem(value);
            });
            $state.go('login', { redirectTo: $state.current.name });
        }
    }

})();

