/**
 * Created by roshansachan on 10/01/17.
 */

(function () {
    'use strict';

    angular
        .module('app.services')
        .service('ProgressBarService', ProgressBarService);



    /* @ngInject */
    function ProgressBarService(ngProgressFactory) {
        this.create = create;
        
        function create(selector) {
            let progressbar = ngProgressFactory.createInstance();
            if (angular.isDefined(selector) && selector !== '') {
                progressbar.setParent($(selector)[0]);
            }
            progressbar.start();
            return progressbar;
        }

    }

})();

