(function () {
    "use strict";

    angular.module('app.filters').filter('trustHtml', trustHtml);

    /* @ngInject */
    function trustHtml($sce) {
        return function (html) {
            return $sce.trustAsHtml(html);
        };
    }

})();