(function () {
    'use strict';

    angular
        .module('app.controllers')
        .controller('LoginCtrl', LoginCtrl);


    /* @ngInject */
    function LoginCtrl($state, $window, SITE_CONSTANTS, ProgressBarService) {
        var vm = this;
        activate();

        function activate() {
            let progress_bar = ProgressBarService.create();
            progress_bar.complete();
        }
    }

})();


