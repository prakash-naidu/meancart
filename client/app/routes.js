(function () {
    "use strict";

    angular
        .module('app.routes')
        .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
            $locationProvider.html5Mode(true);
    
            $urlRouterProvider.otherwise(function($injector, $location){
                let state = $injector.get('$state');
                state.go('404');
                return $location.path();
            });

            $stateProvider

                .state('home', {
                    url: '/',
                    templateUrl: getView('home'),
                    controller: 'HomeCtrl',
                    controllerAs: 'HC',
                    data: {
                        permissions: {
                            only: ['seeDashboard'],
                            redirectTo: {
                                "seeDashboard": 'login'
                            }
                        }
                    }
                })
                
                .state('login', {
                    url: '/login',
                    templateUrl: getView('login'),
                    controller: 'LoginCtrl',
                    controllerAs: 'LC'
                })

                .state('register', {
                    url: '/register',
                    templateUrl: getView('register'),
                    controller: 'RegisterCtrl',
                    controllerAs: 'RC'
                })
                .state('404', {
                    templateUrl: getView('error'),
                });

        });
})();