let mongoose = require('mongoose');
let bcrypt = require('bcrypt-nodejs');

let UserSchema = new mongoose.Schema({
    email:{type:String,required:true,max:100},
    password:{type:String},
    first_name:{type:String},
    last_name:{type:String},
    status:{type:String},
    otp:{type:Number},
    otp_exp:{type:String},
    token:{type:String}
},{ collection: 'users' });

// UserSchema.methods.encryptPassword = function(password){
//     return bcrypt.hashSync(password, bcrypt.genSaltSync(5), null);
// };

// UserSchema.methods.validPassword = function(password){
//     return bcrypt.compareSync(password, this.password);
// };

module.exports = mongoose.model('User',UserSchema); 