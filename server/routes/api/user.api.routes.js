let express = require('express');
let router = express.Router();

let userController = require('../../controllers/user.controller');

router.post('/signup',userController.signUp);

router.post('/verify-otp',userController.verifyOtp);

router.post('/resend-otp',userController.resendOtp);

router.post('/forgot-password',userController.forgotPassword);

router.post('/signin',userController.signIn);

module.exports = router;
