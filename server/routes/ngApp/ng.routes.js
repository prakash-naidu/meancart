let express = require('express');
let router = express.Router();
let path = require('path');

router.use(function(req, res, next){
    res.sendFile(path.join(req.app.get('publicPath'), 'index.html'));
});

module.exports = router;
