let express = require('express');
let router = express.Router();

let apiRoutes = require('./api/api.routes');
let webRoutes = require('./web/web.routes');
let ngRoutes = require('./ngApp/ng.routes');

module.exports = (function () {
    return new Routes();
    
    function Routes() {
        this.initialize = initialize;
    }
    
    function initialize(app){
        app.use('/api',apiRoutes);
        app.use('/',webRoutes);
        app.use('*',ngRoutes);
    }
})();
