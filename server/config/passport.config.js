let passport = require('passport');
let LocalStrategy = require('passport-local').Strategy;

let User = require('../models/user.model');

passport.use('local.signIn',new LocalStrategy({

    usernameField:'email',
    passwordField:'password',
    passReqToCallback : true,
    session: false

    },function(req, email, password, done) {
    
        User.findOne({ email: email }, function (err, user) {
    
            if (err) { return done(err); }
            if (!user) {
                return done(null, false, { message:"USER_NOT_FOUND" });
            }
            if (user.password !== password){
                return done(null, false, { message:"INCORRECT_PASSWORD" });
            }
            return done(null, user);
        });
    }
));