let User = require('../models/user.model');

module.exports =(function(){
    return function UserRepository(){
        this.getUserByEmail = getUserByEmail;
        this.saveUser = saveUser;
        this.updateUser = updateUser;
        this.getUserByID = getUserByID;

    };

    function getUserByEmail(email){
        return User.findOne({email:email});
    }

    function getUserByID(id){
        return User.findOne({_id:id});
    }

    function saveUser(data){
        return User.create(data);
    }

    function updateUser(condition,data){
        return User.findOneAndUpdate(condition,data,{new: true});
    }
})();