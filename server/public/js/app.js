'use strict';

AppPermissionsRun.$inject = ["$rootScope", "PermPermissionStore", "$window"];
(function () {
    "use strict";

    angular.module('app', ['app.constants', 'app.run', 'app.routes', 'app.services', 'app.controllers', 'app.directives', 'app.filters', 'app.factories', 'app.config', 'app.api', 'ui.bootstrap', 'ngSanitize', 'ngProgress']);

    angular.module('app.constants', []);
    angular.module('app.run', []);
    angular.module('app.routes', ['ui.router', 'permission', 'permission.ui']);
    angular.module('app.controllers', []);
    angular.module('app.directives', []);
    angular.module('app.filters', []);
    angular.module('app.factories', []);
    angular.module('app.services', []);
    angular.module('app.config', []);
    angular.module('app.api', []);
})();
// (function () {
//     "use strict";


//     angular.module('app.config')


//         .config(/*@ngInject*/function ($qProvider) {
//             $qProvider.errorOnUnhandledRejections(true);
//         })


//         .config(/*@ngInject*/
//         function ($httpProvider) {
//             $httpProvider.interceptors.push('Interceptor');
//         });


// })();
"use strict";
'use strict';

(function () {
    'use strict';

    angular.module('app.constants').constant("SITE_CONSTANTS", window.SITE_CONSTANTS);
})();
'use strict';

(function () {
    AppCtrl.$inject = ["$rootScope", "$window"];
    angular.module('app.controllers').controller('AppCtrl', AppCtrl);

    /* @ngInject */
    function AppCtrl($rootScope, $window) {

        if ($window.innerWidth < 991) {
            $rootScope.hlkey_use_paceholder = true;
        } else {
            $rootScope.hlkey_use_paceholder = false;
        }
    }
})();
'use strict';

(function () {
    "use strict";

    angular.module('app.routes').config(["$stateProvider", "$urlRouterProvider", "$locationProvider", function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);

        $urlRouterProvider.otherwise(function ($injector, $location) {
            var state = $injector.get('$state');
            state.go('404');
            return $location.path();
        });

        $stateProvider.state('home', {
            url: '/',
            templateUrl: getView('home'),
            controller: 'HomeCtrl',
            controllerAs: 'HC',
            data: {
                permissions: {
                    only: ['seeDashboard'],
                    redirectTo: {
                        "seeDashboard": 'login'
                    }
                }
            }
        }).state('login', {
            url: '/login',
            templateUrl: getView('login'),
            controller: 'LoginCtrl',
            controllerAs: 'LC'
        }).state('register', {
            url: '/register',
            templateUrl: getView('register'),
            controller: 'RegisterCtrl',
            controllerAs: 'RC'
        }).state('404', {
            templateUrl: getView('error')
        });
    }]);
})();
"use strict";

(function () {
    "use strict";

    BoardsAPI.$inject = ["$http", "SITE_CONSTANTS"];
    angular.module("app.api").service('BoardsAPI', BoardsAPI);

    /* @ngInject */
    function BoardsAPI($http, SITE_CONSTANTS) {
        var api_route = SITE_CONSTANTS.api_route;
        var mock_api_route = SITE_CONSTANTS.mock_api_route;

        return {
            list: function list(callback, errorCatch) {
                $http({
                    method: 'GET',
                    url: api_route + 'list-board'
                }).then(callback, errorCatch);
            },
            create: function create(data, callback, errorCatch) {
                $http({
                    method: 'POST',
                    url: api_route + 'admin/board/add',
                    data: data
                }).then(callback, errorCatch);
            },
            update: function update(data, callback, errorCatch) {
                $http({
                    method: 'POST',
                    url: api_route + 'admin/board/update',
                    data: data
                }).then(callback, errorCatch);
            },
            delete: function _delete(data, callback, errorCatch) {
                $http({
                    method: 'POST',
                    url: api_route + 'admin/board/delete',
                    data: data
                }).then(callback, errorCatch);
            }
        };
    }
})();
'use strict';

/**
 * Created by roshansachan on 15/01/17.
 */
(function () {
    "use strict";

    backDirective.$inject = ["$window"];
    angular.module('app.directives').directive('back', backDirective);

    /* @ngInject */
    function backDirective($window) {
        return {
            restrict: 'A',
            link: link
        };

        function link(scope, element, attrs) {
            element.bind('click', function () {
                $window.history.back();
            });
        }
    }
})();
'use strict';

(function () {
    angular.module('validation.rule', ['validation']).config(['$validationProvider', function ($validationProvider) {
        var expression = {
            required: function required(value) {
                return !!value;
            },
            url: /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/,
            email: /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/,
            number: /^\d+$/,
            minlength: function minlength(value, scope, element, attrs, param) {
                return value.length >= param;
            },
            maxlength: function maxlength(value, scope, element, attrs, param) {
                return value.length <= param;
            }
        };

        var defaultMsg = {
            required: {
                error: 'This should be Required!!',
                success: 'It\'s Required'
            },
            url: {
                error: 'This should be Url',
                success: 'It\'s Url'
            },
            email: {
                error: 'This should be Email',
                success: 'It\'s Email'
            },
            number: {
                error: 'This should be Number',
                success: 'It\'s Number'
            },
            minlength: {
                error: 'This should be longer',
                success: 'Long enough!'
            },
            maxlength: {
                error: 'This should be shorter',
                success: 'Short enough!'
            }
        };
        $validationProvider.setExpression(expression).setDefaultMsg(defaultMsg);

        $validationProvider.showSuccessMessage = false;

        $validationProvider.setErrorHTML(function (msg) {
            return "<label class=\"control-label has-error mt-10\">" + msg + "</label>";
        });

        angular.extend($validationProvider, {
            validCallback: function validCallback(element) {
                $(element).parents('.form-group:first').removeClass('has-error');
            },
            invalidCallback: function invalidCallback(element) {
                $(element).parents('.form-group:first').addClass('has-error');
            }
        });

        $validationProvider.setExpression({
            requiredIf: function requiredIf(value, scope, element, attrs) {
                console.log(attrs.requiredif);
                if (attrs.requiredif === 'rejected') {
                    if (value) {
                        return value;
                    }
                    return false;
                }
                return true;
            }
        }).setDefaultMsg({
            requiredIf: {
                error: 'This should be Required!!'
            }
        });
    }]);
}).call(undefined);
'use strict';

(function () {
    "use strict";

    trustHtml.$inject = ["$sce"];
    angular.module('app.filters').filter('trustHtml', trustHtml);

    /* @ngInject */
    function trustHtml($sce) {
        return function (html) {
            return $sce.trustAsHtml(html);
        };
    }
})();
'use strict';

angular.module('app.run').run(AppPermissionsRun);
/* @ngInject */
function AppPermissionsRun($rootScope, PermPermissionStore, $window) {

    PermPermissionStore.definePermission('seeDashboard', function () {
        // return $window.localStorage.getItem('token') ? true : false;
        return true;
    });
}
'use strict';

(function () {
    'use strict';

    LogoutService.$inject = ["$window", "$state"];
    angular.module('app.services').service('LogoutService', LogoutService);

    /* @ngInject */
    function LogoutService($window, $state) {
        this.logout = logout;
        function logout() {
            var local_stogare_item = ['token'];
            angular.forEach(local_stogare_item, function (value, key) {
                $window.localStorage.removeItem(value);
            });
            $state.go('login', { redirectTo: $state.current.name });
        }
    }
})();
'use strict';

/**
 * Created by roshansachan on 10/01/17.
 */

(function () {
    'use strict';

    ProgressBarService.$inject = ["ngProgressFactory"];
    angular.module('app.services').service('ProgressBarService', ProgressBarService);

    /* @ngInject */
    function ProgressBarService(ngProgressFactory) {
        this.create = create;

        function create(selector) {
            var progressbar = ngProgressFactory.createInstance();
            if (angular.isDefined(selector) && selector !== '') {
                progressbar.setParent($(selector)[0]);
            }
            progressbar.start();
            return progressbar;
        }
    }
})();
'use strict';

function isUndefined(data) {
    return typeof data === 'undefined';
}

function getView(moduleName, viewName) {
    if (isUndefined(viewName)) {
        viewName = moduleName;
    }
    return './views/modules/' + moduleName + '/' + viewName + '.html?v=' + window.html_version;
}

function getNestedView(moduleName, viewName) {
    if (isUndefined(viewName)) {
        viewName = moduleName;
    }

    return './views/modules/' + moduleName + '/' + viewName + '/' + viewName + '.html?v=' + window.html_version;
}

function getCommonView(moduleName, viewName) {
    if (isUndefined(viewName)) {
        viewName = moduleName;
    }

    return './views/common/' + moduleName + '/' + viewName + '.html?v=' + window.html_version;
}
'use strict';

(function () {
    'use strict';

    ConfirmationModalCtrl.$inject = ["$scope", "passed_data", "$uibModalInstance"];
    angular.module('app.controllers').controller('ConfirmationModalCtrl', ConfirmationModalCtrl);

    /* @ngInject */
    function ConfirmationModalCtrl($scope, passed_data, $uibModalInstance) {

        var vm = this;
        vm.message = passed_data.message;
        vm.title = passed_data.title;
        vm.show_cancel = false;
        vm.confirmSelection = confirmSelection;
        vm.rejectSelection = rejectSelection;
        vm.cancel = cancel;
        activate();

        function activate() {
            if (angular.isDefined(passed_data.yes_text)) {
                vm.yes_text = passed_data.yes_text;
            } else {
                vm.yes_text = "YES";
            }
            if (angular.isDefined(passed_data.no_text)) {
                vm.no_text = passed_data.no_text;
            } else {
                vm.no_text = "NO";
            }
            if (angular.isDefined(passed_data.cancel_text)) {
                vm.show_cancel = true;
                vm.cancel_text = passed_data.cancel_text;
            }
        }

        function confirmSelection() {
            $uibModalInstance.close({
                status: 'yes'
            });
        }
        function rejectSelection() {
            $uibModalInstance.close({
                status: 'no'
            });
        }

        function cancel() {
            $uibModalInstance.dismiss({
                status: 'cancel'
            });
        }
    }
})();
'use strict';

(function () {
    'use strict';

    HomeCtrl.$inject = ["$state", "$window", "SITE_CONSTANTS", "ProgressBarService"];
    angular.module('app.controllers').controller('HomeCtrl', HomeCtrl);

    /* @ngInject */
    function HomeCtrl($state, $window, SITE_CONSTANTS, ProgressBarService) {
        var vm = this;
        activate();

        function activate() {
            var progress_bar = ProgressBarService.create();
            progress_bar.complete();
        }
    }
})();
'use strict';

(function () {
    'use strict';

    LoginCtrl.$inject = ["$state", "$window", "SITE_CONSTANTS", "ProgressBarService"];
    angular.module('app.controllers').controller('LoginCtrl', LoginCtrl);

    /* @ngInject */
    function LoginCtrl($state, $window, SITE_CONSTANTS, ProgressBarService) {
        var vm = this;
        activate();

        function activate() {
            var progress_bar = ProgressBarService.create();
            progress_bar.complete();
        }
    }
})();
'use strict';

(function () {
    'use strict';

    RegisterCtrl.$inject = ["$state", "$window", "SITE_CONSTANTS", "ProgressBarService"];
    angular.module('app.controllers').controller('RegisterCtrl', RegisterCtrl);

    /* @ngInject */
    function RegisterCtrl($state, $window, SITE_CONSTANTS, ProgressBarService) {
        var vm = this;
        activate();

        function activate() {
            var progress_bar = ProgressBarService.create();
            progress_bar.complete();
        }
    }
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZGV4LmpzIiwiYXBwLmNvbmZpZy5qcyIsImFwcC5jb25zdGFudHMuanMiLCJhcHAuY29udHJvbGxlci5qcyIsInJvdXRlcy5qcyIsImFwaS9ib2FyZHMuYXBpLmpzIiwiZGlyZWN0aXZlcy9iYWNrLmRpcmVjdGl2ZS5qcyIsImV4dGVybmFsL2FuZ3VsYXItdmFsaWRhdGlvbi1ydWxlLmpzIiwiZmlsdGVycy90cnVzdEh0bWwuZmlsdGVyLmpzIiwicnVuL2FwcC5wZXJtaXNzaW9ucy5ydW4uanMiLCJzZXJ2aWNlcy9sb2dvdXQuc2VydmljZS5qcyIsInNlcnZpY2VzL3Byb2dyZXNzQmFyLnNlcnZpY2UuanMiLCJ1dGlscy91dGlscy5qcyIsImNvbW1vbi9jb25maXJtYXRpb24vY29uZmlybWF0aW9uLmNvbnRyb2xsZXIuanMiLCJtb2R1bGVzL2hvbWUvaG9tZS5jb250cm9sbGVyLmpzIiwibW9kdWxlcy9sb2dpbi9sb2dpbi5jb250cm9sbGVyLmpzIiwibW9kdWxlcy9yZWdpc3Rlci9yZWdpc3Rlci5jb250cm9sbGVyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7QUFFQSxDQUFBLFlBQUE7SUFDQTs7SUFFQSxRQUFBLE9BQUEsT0FBQSxDQUFBLGlCQUFBLFdBQUEsY0FBQSxnQkFBQSxtQkFBQSxrQkFBQSxlQUFBLGlCQUFBLGNBQUEsV0FBQSxnQkFBQSxjQUFBOztJQUVBLFFBQUEsT0FBQSxpQkFBQTtJQUNBLFFBQUEsT0FBQSxXQUFBO0lBQ0EsUUFBQSxPQUFBLGNBQUEsQ0FBQSxhQUFBLGNBQUE7SUFDQSxRQUFBLE9BQUEsbUJBQUE7SUFDQSxRQUFBLE9BQUEsa0JBQUE7SUFDQSxRQUFBLE9BQUEsZUFBQTtJQUNBLFFBQUEsT0FBQSxpQkFBQTtJQUNBLFFBQUEsT0FBQSxnQkFBQTtJQUNBLFFBQUEsT0FBQSxjQUFBO0lBQ0EsUUFBQSxPQUFBLFdBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0dBO0FDbkJBOztBQUVBLENBQUEsWUFBQTtJQUNBOztJQUVBLFFBQUEsT0FBQSxpQkFBQSxTQUFBLGtCQUFBLE9BQUE7O0FDTEE7OztpREFFQSxDQUFBLFlBQUE7SUFDQSxRQUFBLE9BQUEsbUJBQUEsV0FBQSxXQUFBOzs7SUFHQSxTQUFBLFFBQUEsWUFBQSxTQUFBOztRQUVBLElBQUEsUUFBQSxhQUFBLEtBQUE7WUFDQSxXQUFBLHVCQUFBO2VBQ0E7WUFDQSxXQUFBLHVCQUFBOzs7O0FDWEE7O0FBRUEsQ0FBQSxZQUFBO0lBQ0E7O0lBRUEsUUFBQSxPQUFBLGNBQUEscUVBQUEsVUFBQSxnQkFBQSxvQkFBQSxtQkFBQTtRQUNBLGtCQUFBLFVBQUE7O1FBRUEsbUJBQUEsVUFBQSxVQUFBLFdBQUEsV0FBQTtZQUNBLElBQUEsUUFBQSxVQUFBLElBQUE7WUFDQSxNQUFBLEdBQUE7WUFDQSxPQUFBLFVBQUE7OztRQUdBLGVBQUEsTUFBQSxRQUFBO1lBQ0EsS0FBQTtZQUNBLGFBQUEsUUFBQTtZQUNBLFlBQUE7WUFDQSxjQUFBO1lBQ0EsTUFBQTtnQkFDQSxhQUFBO29CQUNBLE1BQUEsQ0FBQTtvQkFDQSxZQUFBO3dCQUNBLGdCQUFBOzs7O1dBSUEsTUFBQSxTQUFBO1lBQ0EsS0FBQTtZQUNBLGFBQUEsUUFBQTtZQUNBLFlBQUE7WUFDQSxjQUFBO1dBQ0EsTUFBQSxZQUFBO1lBQ0EsS0FBQTtZQUNBLGFBQUEsUUFBQTtZQUNBLFlBQUE7WUFDQSxjQUFBO1dBQ0EsTUFBQSxPQUFBO1lBQ0EsYUFBQSxRQUFBOzs7O0FDdENBOztBQUVBLENBQUEsWUFBQTtJQUNBOzs7SUFFQSxRQUFBLE9BQUEsV0FBQSxRQUFBLGFBQUE7OztJQUdBLFNBQUEsVUFBQSxPQUFBLGdCQUFBO1FBQ0EsSUFBQSxZQUFBLGVBQUE7UUFDQSxJQUFBLGlCQUFBLGVBQUE7O1FBRUEsT0FBQTtZQUNBLE1BQUEsU0FBQSxLQUFBLFVBQUEsWUFBQTtnQkFDQSxNQUFBO29CQUNBLFFBQUE7b0JBQ0EsS0FBQSxZQUFBO21CQUNBLEtBQUEsVUFBQTs7WUFFQSxRQUFBLFNBQUEsT0FBQSxNQUFBLFVBQUEsWUFBQTtnQkFDQSxNQUFBO29CQUNBLFFBQUE7b0JBQ0EsS0FBQSxZQUFBO29CQUNBLE1BQUE7bUJBQ0EsS0FBQSxVQUFBOztZQUVBLFFBQUEsU0FBQSxPQUFBLE1BQUEsVUFBQSxZQUFBO2dCQUNBLE1BQUE7b0JBQ0EsUUFBQTtvQkFDQSxLQUFBLFlBQUE7b0JBQ0EsTUFBQTttQkFDQSxLQUFBLFVBQUE7O1lBRUEsUUFBQSxTQUFBLFFBQUEsTUFBQSxVQUFBLFlBQUE7Z0JBQ0EsTUFBQTtvQkFDQSxRQUFBO29CQUNBLEtBQUEsWUFBQTtvQkFDQSxNQUFBO21CQUNBLEtBQUEsVUFBQTs7Ozs7QUN0Q0E7Ozs7O0FBS0EsQ0FBQSxZQUFBO0lBQ0E7OztJQUVBLFFBQUEsT0FBQSxrQkFBQSxVQUFBLFFBQUE7OztJQUdBLFNBQUEsY0FBQSxTQUFBO1FBQ0EsT0FBQTtZQUNBLFVBQUE7WUFDQSxNQUFBOzs7UUFHQSxTQUFBLEtBQUEsT0FBQSxTQUFBLE9BQUE7WUFDQSxRQUFBLEtBQUEsU0FBQSxZQUFBO2dCQUNBLFFBQUEsUUFBQTs7Ozs7QUNuQkE7O0FBRUEsQ0FBQSxZQUFBO0lBQ0EsUUFBQSxPQUFBLG1CQUFBLENBQUEsZUFBQSxPQUFBLENBQUEsdUJBQUEsVUFBQSxxQkFBQTtRQUNBLElBQUEsYUFBQTtZQUNBLFVBQUEsU0FBQSxTQUFBLE9BQUE7Z0JBQ0EsT0FBQSxDQUFBLENBQUE7O1lBRUEsS0FBQTtZQUNBLE9BQUE7WUFDQSxRQUFBO1lBQ0EsV0FBQSxTQUFBLFVBQUEsT0FBQSxPQUFBLFNBQUEsT0FBQSxPQUFBO2dCQUNBLE9BQUEsTUFBQSxVQUFBOztZQUVBLFdBQUEsU0FBQSxVQUFBLE9BQUEsT0FBQSxTQUFBLE9BQUEsT0FBQTtnQkFDQSxPQUFBLE1BQUEsVUFBQTs7OztRQUlBLElBQUEsYUFBQTtZQUNBLFVBQUE7Z0JBQ0EsT0FBQTtnQkFDQSxTQUFBOztZQUVBLEtBQUE7Z0JBQ0EsT0FBQTtnQkFDQSxTQUFBOztZQUVBLE9BQUE7Z0JBQ0EsT0FBQTtnQkFDQSxTQUFBOztZQUVBLFFBQUE7Z0JBQ0EsT0FBQTtnQkFDQSxTQUFBOztZQUVBLFdBQUE7Z0JBQ0EsT0FBQTtnQkFDQSxTQUFBOztZQUVBLFdBQUE7Z0JBQ0EsT0FBQTtnQkFDQSxTQUFBOzs7UUFHQSxvQkFBQSxjQUFBLFlBQUEsY0FBQTs7UUFFQSxvQkFBQSxxQkFBQTs7UUFFQSxvQkFBQSxhQUFBLFVBQUEsS0FBQTtZQUNBLE9BQUEsb0RBQUEsTUFBQTs7O1FBR0EsUUFBQSxPQUFBLHFCQUFBO1lBQ0EsZUFBQSxTQUFBLGNBQUEsU0FBQTtnQkFDQSxFQUFBLFNBQUEsUUFBQSxxQkFBQSxZQUFBOztZQUVBLGlCQUFBLFNBQUEsZ0JBQUEsU0FBQTtnQkFDQSxFQUFBLFNBQUEsUUFBQSxxQkFBQSxTQUFBOzs7O1FBSUEsb0JBQUEsY0FBQTtZQUNBLFlBQUEsU0FBQSxXQUFBLE9BQUEsT0FBQSxTQUFBLE9BQUE7Z0JBQ0EsUUFBQSxJQUFBLE1BQUE7Z0JBQ0EsSUFBQSxNQUFBLGVBQUEsWUFBQTtvQkFDQSxJQUFBLE9BQUE7d0JBQ0EsT0FBQTs7b0JBRUEsT0FBQTs7Z0JBRUEsT0FBQTs7V0FFQSxjQUFBO1lBQ0EsWUFBQTtnQkFDQSxPQUFBOzs7O0dBSUEsS0FBQTtBQy9FQTs7QUFFQSxDQUFBLFlBQUE7SUFDQTs7O0lBRUEsUUFBQSxPQUFBLGVBQUEsT0FBQSxhQUFBOzs7SUFHQSxTQUFBLFVBQUEsTUFBQTtRQUNBLE9BQUEsVUFBQSxNQUFBO1lBQ0EsT0FBQSxLQUFBLFlBQUE7Ozs7QUNWQTs7QUFFQSxRQUFBLE9BQUEsV0FBQSxJQUFBOztBQUVBLFNBQUEsa0JBQUEsWUFBQSxxQkFBQSxTQUFBOztJQUVBLG9CQUFBLGlCQUFBLGdCQUFBLFlBQUE7O1FBRUEsT0FBQTs7O0FDUkE7O0FBRUEsQ0FBQSxZQUFBO0lBQ0E7OztJQUVBLFFBQUEsT0FBQSxnQkFBQSxRQUFBLGlCQUFBOzs7SUFHQSxTQUFBLGNBQUEsU0FBQSxRQUFBO1FBQ0EsS0FBQSxTQUFBO1FBQ0EsU0FBQSxTQUFBO1lBQ0EsSUFBQSxxQkFBQSxDQUFBO1lBQ0EsUUFBQSxRQUFBLG9CQUFBLFVBQUEsT0FBQSxLQUFBO2dCQUNBLFFBQUEsYUFBQSxXQUFBOztZQUVBLE9BQUEsR0FBQSxTQUFBLEVBQUEsWUFBQSxPQUFBLFFBQUE7Ozs7QUNmQTs7Ozs7O0FBTUEsQ0FBQSxZQUFBO0lBQ0E7OztJQUVBLFFBQUEsT0FBQSxnQkFBQSxRQUFBLHNCQUFBOzs7SUFHQSxTQUFBLG1CQUFBLG1CQUFBO1FBQ0EsS0FBQSxTQUFBOztRQUVBLFNBQUEsT0FBQSxVQUFBO1lBQ0EsSUFBQSxjQUFBLGtCQUFBO1lBQ0EsSUFBQSxRQUFBLFVBQUEsYUFBQSxhQUFBLElBQUE7Z0JBQ0EsWUFBQSxVQUFBLEVBQUEsVUFBQTs7WUFFQSxZQUFBO1lBQ0EsT0FBQTs7OztBQ3JCQTs7QUFFQSxTQUFBLFlBQUEsTUFBQTtJQUNBLE9BQUEsT0FBQSxTQUFBOzs7QUFHQSxTQUFBLFFBQUEsWUFBQSxVQUFBO0lBQ0EsSUFBQSxZQUFBLFdBQUE7UUFDQSxXQUFBOztJQUVBLE9BQUEscUJBQUEsYUFBQSxNQUFBLFdBQUEsYUFBQSxPQUFBOzs7QUFHQSxTQUFBLGNBQUEsWUFBQSxVQUFBO0lBQ0EsSUFBQSxZQUFBLFdBQUE7UUFDQSxXQUFBOzs7SUFHQSxPQUFBLHFCQUFBLGFBQUEsTUFBQSxXQUFBLE1BQUEsV0FBQSxhQUFBLE9BQUE7OztBQUdBLFNBQUEsY0FBQSxZQUFBLFVBQUE7SUFDQSxJQUFBLFlBQUEsV0FBQTtRQUNBLFdBQUE7OztJQUdBLE9BQUEsb0JBQUEsYUFBQSxNQUFBLFdBQUEsYUFBQSxPQUFBOztBQzFCQTs7QUFFQSxDQUFBLFlBQUE7SUFDQTs7O0lBRUEsUUFBQSxPQUFBLG1CQUFBLFdBQUEseUJBQUE7OztJQUdBLFNBQUEsc0JBQUEsUUFBQSxhQUFBLG1CQUFBOztRQUVBLElBQUEsS0FBQTtRQUNBLEdBQUEsVUFBQSxZQUFBO1FBQ0EsR0FBQSxRQUFBLFlBQUE7UUFDQSxHQUFBLGNBQUE7UUFDQSxHQUFBLG1CQUFBO1FBQ0EsR0FBQSxrQkFBQTtRQUNBLEdBQUEsU0FBQTtRQUNBOztRQUVBLFNBQUEsV0FBQTtZQUNBLElBQUEsUUFBQSxVQUFBLFlBQUEsV0FBQTtnQkFDQSxHQUFBLFdBQUEsWUFBQTttQkFDQTtnQkFDQSxHQUFBLFdBQUE7O1lBRUEsSUFBQSxRQUFBLFVBQUEsWUFBQSxVQUFBO2dCQUNBLEdBQUEsVUFBQSxZQUFBO21CQUNBO2dCQUNBLEdBQUEsVUFBQTs7WUFFQSxJQUFBLFFBQUEsVUFBQSxZQUFBLGNBQUE7Z0JBQ0EsR0FBQSxjQUFBO2dCQUNBLEdBQUEsY0FBQSxZQUFBOzs7O1FBSUEsU0FBQSxtQkFBQTtZQUNBLGtCQUFBLE1BQUE7Z0JBQ0EsUUFBQTs7O1FBR0EsU0FBQSxrQkFBQTtZQUNBLGtCQUFBLE1BQUE7Z0JBQ0EsUUFBQTs7OztRQUlBLFNBQUEsU0FBQTtZQUNBLGtCQUFBLFFBQUE7Z0JBQ0EsUUFBQTs7Ozs7QUNqREE7O0FBRUEsQ0FBQSxZQUFBO0lBQ0E7OztJQUVBLFFBQUEsT0FBQSxtQkFBQSxXQUFBLFlBQUE7OztJQUdBLFNBQUEsU0FBQSxRQUFBLFNBQUEsZ0JBQUEsb0JBQUE7UUFDQSxJQUFBLEtBQUE7UUFDQTs7UUFFQSxTQUFBLFdBQUE7WUFDQSxJQUFBLGVBQUEsbUJBQUE7WUFDQSxhQUFBOzs7O0FDZEE7O0FBRUEsQ0FBQSxZQUFBO0lBQ0E7OztJQUVBLFFBQUEsT0FBQSxtQkFBQSxXQUFBLGFBQUE7OztJQUdBLFNBQUEsVUFBQSxRQUFBLFNBQUEsZ0JBQUEsb0JBQUE7UUFDQSxJQUFBLEtBQUE7UUFDQTs7UUFFQSxTQUFBLFdBQUE7WUFDQSxJQUFBLGVBQUEsbUJBQUE7WUFDQSxhQUFBOzs7O0FDZEE7O0FBRUEsQ0FBQSxZQUFBO0lBQ0E7OztJQUVBLFFBQUEsT0FBQSxtQkFBQSxXQUFBLGdCQUFBOzs7SUFHQSxTQUFBLGFBQUEsUUFBQSxTQUFBLGdCQUFBLG9CQUFBO1FBQ0EsSUFBQSxLQUFBO1FBQ0E7O1FBRUEsU0FBQSxXQUFBO1lBQ0EsSUFBQSxlQUFBLG1CQUFBO1lBQ0EsYUFBQTs7O0tBR0EiLCJmaWxlIjoiYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG4oZnVuY3Rpb24gKCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcCcsIFsnYXBwLmNvbnN0YW50cycsICdhcHAucnVuJywgJ2FwcC5yb3V0ZXMnLCAnYXBwLnNlcnZpY2VzJywgJ2FwcC5jb250cm9sbGVycycsICdhcHAuZGlyZWN0aXZlcycsICdhcHAuZmlsdGVycycsICdhcHAuZmFjdG9yaWVzJywgJ2FwcC5jb25maWcnLCAnYXBwLmFwaScsICd1aS5ib290c3RyYXAnLCAnbmdTYW5pdGl6ZScsICduZ1Byb2dyZXNzJ10pO1xuXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb25zdGFudHMnLCBbXSk7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5ydW4nLCBbXSk7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5yb3V0ZXMnLCBbJ3VpLnJvdXRlcicsICdwZXJtaXNzaW9uJywgJ3Blcm1pc3Npb24udWknXSk7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb250cm9sbGVycycsIFtdKTtcbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLmRpcmVjdGl2ZXMnLCBbXSk7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5maWx0ZXJzJywgW10pO1xuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuZmFjdG9yaWVzJywgW10pO1xuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuc2VydmljZXMnLCBbXSk7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb25maWcnLCBbXSk7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5hcGknLCBbXSk7XG59KSgpOyIsIi8vIChmdW5jdGlvbiAoKSB7XG4vLyAgICAgXCJ1c2Ugc3RyaWN0XCI7XG5cblxuLy8gICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuY29uZmlnJylcblxuXG4vLyAgICAgICAgIC5jb25maWcoLypAbmdJbmplY3QqL2Z1bmN0aW9uICgkcVByb3ZpZGVyKSB7XG4vLyAgICAgICAgICAgICAkcVByb3ZpZGVyLmVycm9yT25VbmhhbmRsZWRSZWplY3Rpb25zKHRydWUpO1xuLy8gICAgICAgICB9KVxuXG5cbi8vICAgICAgICAgLmNvbmZpZygvKkBuZ0luamVjdCovXG4vLyAgICAgICAgIGZ1bmN0aW9uICgkaHR0cFByb3ZpZGVyKSB7XG4vLyAgICAgICAgICAgICAkaHR0cFByb3ZpZGVyLmludGVyY2VwdG9ycy5wdXNoKCdJbnRlcmNlcHRvcicpO1xuLy8gICAgICAgICB9KTtcblxuXG4vLyB9KSgpO1xuXCJ1c2Ugc3RyaWN0XCI7IiwiJ3VzZSBzdHJpY3QnO1xuXG4oZnVuY3Rpb24gKCkge1xuICAgICd1c2Ugc3RyaWN0JztcblxuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuY29uc3RhbnRzJykuY29uc3RhbnQoXCJTSVRFX0NPTlNUQU5UU1wiLCB3aW5kb3cuU0lURV9DT05TVEFOVFMpO1xufSkoKTsiLCIndXNlIHN0cmljdCc7XG5cbihmdW5jdGlvbiAoKSB7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb250cm9sbGVycycpLmNvbnRyb2xsZXIoJ0FwcEN0cmwnLCBBcHBDdHJsKTtcblxuICAgIC8qIEBuZ0luamVjdCAqL1xuICAgIGZ1bmN0aW9uIEFwcEN0cmwoJHJvb3RTY29wZSwgJHdpbmRvdykge1xuXG4gICAgICAgIGlmICgkd2luZG93LmlubmVyV2lkdGggPCA5OTEpIHtcbiAgICAgICAgICAgICRyb290U2NvcGUuaGxrZXlfdXNlX3BhY2Vob2xkZXIgPSB0cnVlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgJHJvb3RTY29wZS5obGtleV91c2VfcGFjZWhvbGRlciA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgfVxufSkoKTsiLCIndXNlIHN0cmljdCc7XG5cbihmdW5jdGlvbiAoKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLnJvdXRlcycpLmNvbmZpZyhmdW5jdGlvbiAoJHN0YXRlUHJvdmlkZXIsICR1cmxSb3V0ZXJQcm92aWRlciwgJGxvY2F0aW9uUHJvdmlkZXIpIHtcbiAgICAgICAgJGxvY2F0aW9uUHJvdmlkZXIuaHRtbDVNb2RlKHRydWUpO1xuXG4gICAgICAgICR1cmxSb3V0ZXJQcm92aWRlci5vdGhlcndpc2UoZnVuY3Rpb24gKCRpbmplY3RvciwgJGxvY2F0aW9uKSB7XG4gICAgICAgICAgICB2YXIgc3RhdGUgPSAkaW5qZWN0b3IuZ2V0KCckc3RhdGUnKTtcbiAgICAgICAgICAgIHN0YXRlLmdvKCc0MDQnKTtcbiAgICAgICAgICAgIHJldHVybiAkbG9jYXRpb24ucGF0aCgpO1xuICAgICAgICB9KTtcblxuICAgICAgICAkc3RhdGVQcm92aWRlci5zdGF0ZSgnaG9tZScsIHtcbiAgICAgICAgICAgIHVybDogJy8nLFxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6IGdldFZpZXcoJ2hvbWUnKSxcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6ICdIb21lQ3RybCcsXG4gICAgICAgICAgICBjb250cm9sbGVyQXM6ICdIQycsXG4gICAgICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICAgICAgcGVybWlzc2lvbnM6IHtcbiAgICAgICAgICAgICAgICAgICAgb25seTogWydzZWVEYXNoYm9hcmQnXSxcbiAgICAgICAgICAgICAgICAgICAgcmVkaXJlY3RUbzoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJzZWVEYXNoYm9hcmRcIjogJ2xvZ2luJ1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KS5zdGF0ZSgnbG9naW4nLCB7XG4gICAgICAgICAgICB1cmw6ICcvbG9naW4nLFxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6IGdldFZpZXcoJ2xvZ2luJyksXG4gICAgICAgICAgICBjb250cm9sbGVyOiAnTG9naW5DdHJsJyxcbiAgICAgICAgICAgIGNvbnRyb2xsZXJBczogJ0xDJ1xuICAgICAgICB9KS5zdGF0ZSgncmVnaXN0ZXInLCB7XG4gICAgICAgICAgICB1cmw6ICcvcmVnaXN0ZXInLFxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6IGdldFZpZXcoJ3JlZ2lzdGVyJyksXG4gICAgICAgICAgICBjb250cm9sbGVyOiAnUmVnaXN0ZXJDdHJsJyxcbiAgICAgICAgICAgIGNvbnRyb2xsZXJBczogJ1JDJ1xuICAgICAgICB9KS5zdGF0ZSgnNDA0Jywge1xuICAgICAgICAgICAgdGVtcGxhdGVVcmw6IGdldFZpZXcoJ2Vycm9yJylcbiAgICAgICAgfSk7XG4gICAgfSk7XG59KSgpOyIsIlwidXNlIHN0cmljdFwiO1xuXG4oZnVuY3Rpb24gKCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgYW5ndWxhci5tb2R1bGUoXCJhcHAuYXBpXCIpLnNlcnZpY2UoJ0JvYXJkc0FQSScsIEJvYXJkc0FQSSk7XG5cbiAgICAvKiBAbmdJbmplY3QgKi9cbiAgICBmdW5jdGlvbiBCb2FyZHNBUEkoJGh0dHAsIFNJVEVfQ09OU1RBTlRTKSB7XG4gICAgICAgIHZhciBhcGlfcm91dGUgPSBTSVRFX0NPTlNUQU5UUy5hcGlfcm91dGU7XG4gICAgICAgIHZhciBtb2NrX2FwaV9yb3V0ZSA9IFNJVEVfQ09OU1RBTlRTLm1vY2tfYXBpX3JvdXRlO1xuXG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBsaXN0OiBmdW5jdGlvbiBsaXN0KGNhbGxiYWNrLCBlcnJvckNhdGNoKSB7XG4gICAgICAgICAgICAgICAgJGh0dHAoe1xuICAgICAgICAgICAgICAgICAgICBtZXRob2Q6ICdHRVQnLFxuICAgICAgICAgICAgICAgICAgICB1cmw6IGFwaV9yb3V0ZSArICdsaXN0LWJvYXJkJ1xuICAgICAgICAgICAgICAgIH0pLnRoZW4oY2FsbGJhY2ssIGVycm9yQ2F0Y2gpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGNyZWF0ZTogZnVuY3Rpb24gY3JlYXRlKGRhdGEsIGNhbGxiYWNrLCBlcnJvckNhdGNoKSB7XG4gICAgICAgICAgICAgICAgJGh0dHAoe1xuICAgICAgICAgICAgICAgICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgICAgICAgICAgICAgICAgdXJsOiBhcGlfcm91dGUgKyAnYWRtaW4vYm9hcmQvYWRkJyxcbiAgICAgICAgICAgICAgICAgICAgZGF0YTogZGF0YVxuICAgICAgICAgICAgICAgIH0pLnRoZW4oY2FsbGJhY2ssIGVycm9yQ2F0Y2gpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHVwZGF0ZTogZnVuY3Rpb24gdXBkYXRlKGRhdGEsIGNhbGxiYWNrLCBlcnJvckNhdGNoKSB7XG4gICAgICAgICAgICAgICAgJGh0dHAoe1xuICAgICAgICAgICAgICAgICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgICAgICAgICAgICAgICAgdXJsOiBhcGlfcm91dGUgKyAnYWRtaW4vYm9hcmQvdXBkYXRlJyxcbiAgICAgICAgICAgICAgICAgICAgZGF0YTogZGF0YVxuICAgICAgICAgICAgICAgIH0pLnRoZW4oY2FsbGJhY2ssIGVycm9yQ2F0Y2gpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGRlbGV0ZTogZnVuY3Rpb24gX2RlbGV0ZShkYXRhLCBjYWxsYmFjaywgZXJyb3JDYXRjaCkge1xuICAgICAgICAgICAgICAgICRodHRwKHtcbiAgICAgICAgICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICAgICAgICAgICAgICAgIHVybDogYXBpX3JvdXRlICsgJ2FkbWluL2JvYXJkL2RlbGV0ZScsXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IGRhdGFcbiAgICAgICAgICAgICAgICB9KS50aGVuKGNhbGxiYWNrLCBlcnJvckNhdGNoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICB9XG59KSgpOyIsIid1c2Ugc3RyaWN0JztcblxuLyoqXHJcbiAqIENyZWF0ZWQgYnkgcm9zaGFuc2FjaGFuIG9uIDE1LzAxLzE3LlxyXG4gKi9cbihmdW5jdGlvbiAoKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLmRpcmVjdGl2ZXMnKS5kaXJlY3RpdmUoJ2JhY2snLCBiYWNrRGlyZWN0aXZlKTtcblxuICAgIC8qIEBuZ0luamVjdCAqL1xuICAgIGZ1bmN0aW9uIGJhY2tEaXJlY3RpdmUoJHdpbmRvdykge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcmVzdHJpY3Q6ICdBJyxcbiAgICAgICAgICAgIGxpbms6IGxpbmtcbiAgICAgICAgfTtcblxuICAgICAgICBmdW5jdGlvbiBsaW5rKHNjb3BlLCBlbGVtZW50LCBhdHRycykge1xuICAgICAgICAgICAgZWxlbWVudC5iaW5kKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAkd2luZG93Lmhpc3RvcnkuYmFjaygpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG59KSgpOyIsIid1c2Ugc3RyaWN0JztcblxuKGZ1bmN0aW9uICgpIHtcbiAgICBhbmd1bGFyLm1vZHVsZSgndmFsaWRhdGlvbi5ydWxlJywgWyd2YWxpZGF0aW9uJ10pLmNvbmZpZyhbJyR2YWxpZGF0aW9uUHJvdmlkZXInLCBmdW5jdGlvbiAoJHZhbGlkYXRpb25Qcm92aWRlcikge1xuICAgICAgICB2YXIgZXhwcmVzc2lvbiA9IHtcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmdW5jdGlvbiByZXF1aXJlZCh2YWx1ZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiAhIXZhbHVlO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHVybDogLygoKFtBLVphLXpdezMsOX06KD86XFwvXFwvKT8pKD86Wy07OiY9XFwrXFwkLFxcd10rQCk/W0EtWmEtejAtOS4tXSt8KD86d3d3LnxbLTs6Jj1cXCtcXCQsXFx3XStAKVtBLVphLXowLTkuLV0rKSgoPzpcXC9bXFwrfiVcXC8uXFx3LV9dKik/XFw/Pyg/OlstXFwrPSY7JUAuXFx3X10qKSM/KD86W1xcd10qKSk/KS8sXG4gICAgICAgICAgICBlbWFpbDogL14oW1xcdy1cXC5dKylAKChcXFtbMC05XXsxLDN9XFwuWzAtOV17MSwzfVxcLlswLTldezEsM31cXC4pfCgoW1xcdy1dK1xcLikrKSkoW2EtekEtWl17Miw0fXxbMC05XXsxLDN9KShcXF0/KSQvLFxuICAgICAgICAgICAgbnVtYmVyOiAvXlxcZCskLyxcbiAgICAgICAgICAgIG1pbmxlbmd0aDogZnVuY3Rpb24gbWlubGVuZ3RoKHZhbHVlLCBzY29wZSwgZWxlbWVudCwgYXR0cnMsIHBhcmFtKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHZhbHVlLmxlbmd0aCA+PSBwYXJhbTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBtYXhsZW5ndGg6IGZ1bmN0aW9uIG1heGxlbmd0aCh2YWx1ZSwgc2NvcGUsIGVsZW1lbnQsIGF0dHJzLCBwYXJhbSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB2YWx1ZS5sZW5ndGggPD0gcGFyYW07XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgdmFyIGRlZmF1bHRNc2cgPSB7XG4gICAgICAgICAgICByZXF1aXJlZDoge1xuICAgICAgICAgICAgICAgIGVycm9yOiAnVGhpcyBzaG91bGQgYmUgUmVxdWlyZWQhIScsXG4gICAgICAgICAgICAgICAgc3VjY2VzczogJ0l0XFwncyBSZXF1aXJlZCdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB1cmw6IHtcbiAgICAgICAgICAgICAgICBlcnJvcjogJ1RoaXMgc2hvdWxkIGJlIFVybCcsXG4gICAgICAgICAgICAgICAgc3VjY2VzczogJ0l0XFwncyBVcmwnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZW1haWw6IHtcbiAgICAgICAgICAgICAgICBlcnJvcjogJ1RoaXMgc2hvdWxkIGJlIEVtYWlsJyxcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiAnSXRcXCdzIEVtYWlsJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIG51bWJlcjoge1xuICAgICAgICAgICAgICAgIGVycm9yOiAnVGhpcyBzaG91bGQgYmUgTnVtYmVyJyxcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiAnSXRcXCdzIE51bWJlcidcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBtaW5sZW5ndGg6IHtcbiAgICAgICAgICAgICAgICBlcnJvcjogJ1RoaXMgc2hvdWxkIGJlIGxvbmdlcicsXG4gICAgICAgICAgICAgICAgc3VjY2VzczogJ0xvbmcgZW5vdWdoISdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBtYXhsZW5ndGg6IHtcbiAgICAgICAgICAgICAgICBlcnJvcjogJ1RoaXMgc2hvdWxkIGJlIHNob3J0ZXInLFxuICAgICAgICAgICAgICAgIHN1Y2Nlc3M6ICdTaG9ydCBlbm91Z2ghJ1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICAkdmFsaWRhdGlvblByb3ZpZGVyLnNldEV4cHJlc3Npb24oZXhwcmVzc2lvbikuc2V0RGVmYXVsdE1zZyhkZWZhdWx0TXNnKTtcblxuICAgICAgICAkdmFsaWRhdGlvblByb3ZpZGVyLnNob3dTdWNjZXNzTWVzc2FnZSA9IGZhbHNlO1xuXG4gICAgICAgICR2YWxpZGF0aW9uUHJvdmlkZXIuc2V0RXJyb3JIVE1MKGZ1bmN0aW9uIChtc2cpIHtcbiAgICAgICAgICAgIHJldHVybiBcIjxsYWJlbCBjbGFzcz1cXFwiY29udHJvbC1sYWJlbCBoYXMtZXJyb3IgbXQtMTBcXFwiPlwiICsgbXNnICsgXCI8L2xhYmVsPlwiO1xuICAgICAgICB9KTtcblxuICAgICAgICBhbmd1bGFyLmV4dGVuZCgkdmFsaWRhdGlvblByb3ZpZGVyLCB7XG4gICAgICAgICAgICB2YWxpZENhbGxiYWNrOiBmdW5jdGlvbiB2YWxpZENhbGxiYWNrKGVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICAkKGVsZW1lbnQpLnBhcmVudHMoJy5mb3JtLWdyb3VwOmZpcnN0JykucmVtb3ZlQ2xhc3MoJ2hhcy1lcnJvcicpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGludmFsaWRDYWxsYmFjazogZnVuY3Rpb24gaW52YWxpZENhbGxiYWNrKGVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICAkKGVsZW1lbnQpLnBhcmVudHMoJy5mb3JtLWdyb3VwOmZpcnN0JykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAkdmFsaWRhdGlvblByb3ZpZGVyLnNldEV4cHJlc3Npb24oe1xuICAgICAgICAgICAgcmVxdWlyZWRJZjogZnVuY3Rpb24gcmVxdWlyZWRJZih2YWx1ZSwgc2NvcGUsIGVsZW1lbnQsIGF0dHJzKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coYXR0cnMucmVxdWlyZWRpZik7XG4gICAgICAgICAgICAgICAgaWYgKGF0dHJzLnJlcXVpcmVkaWYgPT09ICdyZWplY3RlZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSkuc2V0RGVmYXVsdE1zZyh7XG4gICAgICAgICAgICByZXF1aXJlZElmOiB7XG4gICAgICAgICAgICAgICAgZXJyb3I6ICdUaGlzIHNob3VsZCBiZSBSZXF1aXJlZCEhJ1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XSk7XG59KS5jYWxsKHVuZGVmaW5lZCk7IiwiJ3VzZSBzdHJpY3QnO1xuXG4oZnVuY3Rpb24gKCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5maWx0ZXJzJykuZmlsdGVyKCd0cnVzdEh0bWwnLCB0cnVzdEh0bWwpO1xuXG4gICAgLyogQG5nSW5qZWN0ICovXG4gICAgZnVuY3Rpb24gdHJ1c3RIdG1sKCRzY2UpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uIChodG1sKSB7XG4gICAgICAgICAgICByZXR1cm4gJHNjZS50cnVzdEFzSHRtbChodG1sKTtcbiAgICAgICAgfTtcbiAgICB9XG59KSgpOyIsIid1c2Ugc3RyaWN0JztcblxuYW5ndWxhci5tb2R1bGUoJ2FwcC5ydW4nKS5ydW4oQXBwUGVybWlzc2lvbnNSdW4pO1xuLyogQG5nSW5qZWN0ICovXG5mdW5jdGlvbiBBcHBQZXJtaXNzaW9uc1J1bigkcm9vdFNjb3BlLCBQZXJtUGVybWlzc2lvblN0b3JlLCAkd2luZG93KSB7XG5cbiAgICBQZXJtUGVybWlzc2lvblN0b3JlLmRlZmluZVBlcm1pc3Npb24oJ3NlZURhc2hib2FyZCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgLy8gcmV0dXJuICR3aW5kb3cubG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Rva2VuJykgPyB0cnVlIDogZmFsc2U7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH0pO1xufSIsIid1c2Ugc3RyaWN0JztcblxuKGZ1bmN0aW9uICgpIHtcbiAgICAndXNlIHN0cmljdCc7XG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLnNlcnZpY2VzJykuc2VydmljZSgnTG9nb3V0U2VydmljZScsIExvZ291dFNlcnZpY2UpO1xuXG4gICAgLyogQG5nSW5qZWN0ICovXG4gICAgZnVuY3Rpb24gTG9nb3V0U2VydmljZSgkd2luZG93LCAkc3RhdGUpIHtcbiAgICAgICAgdGhpcy5sb2dvdXQgPSBsb2dvdXQ7XG4gICAgICAgIGZ1bmN0aW9uIGxvZ291dCgpIHtcbiAgICAgICAgICAgIHZhciBsb2NhbF9zdG9nYXJlX2l0ZW0gPSBbJ3Rva2VuJ107XG4gICAgICAgICAgICBhbmd1bGFyLmZvckVhY2gobG9jYWxfc3RvZ2FyZV9pdGVtLCBmdW5jdGlvbiAodmFsdWUsIGtleSkge1xuICAgICAgICAgICAgICAgICR3aW5kb3cubG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0odmFsdWUpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAkc3RhdGUuZ28oJ2xvZ2luJywgeyByZWRpcmVjdFRvOiAkc3RhdGUuY3VycmVudC5uYW1lIH0pO1xuICAgICAgICB9XG4gICAgfVxufSkoKTsiLCIndXNlIHN0cmljdCc7XG5cbi8qKlxyXG4gKiBDcmVhdGVkIGJ5IHJvc2hhbnNhY2hhbiBvbiAxMC8wMS8xNy5cclxuICovXG5cbihmdW5jdGlvbiAoKSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5zZXJ2aWNlcycpLnNlcnZpY2UoJ1Byb2dyZXNzQmFyU2VydmljZScsIFByb2dyZXNzQmFyU2VydmljZSk7XG5cbiAgICAvKiBAbmdJbmplY3QgKi9cbiAgICBmdW5jdGlvbiBQcm9ncmVzc0JhclNlcnZpY2UobmdQcm9ncmVzc0ZhY3RvcnkpIHtcbiAgICAgICAgdGhpcy5jcmVhdGUgPSBjcmVhdGU7XG5cbiAgICAgICAgZnVuY3Rpb24gY3JlYXRlKHNlbGVjdG9yKSB7XG4gICAgICAgICAgICB2YXIgcHJvZ3Jlc3NiYXIgPSBuZ1Byb2dyZXNzRmFjdG9yeS5jcmVhdGVJbnN0YW5jZSgpO1xuICAgICAgICAgICAgaWYgKGFuZ3VsYXIuaXNEZWZpbmVkKHNlbGVjdG9yKSAmJiBzZWxlY3RvciAhPT0gJycpIHtcbiAgICAgICAgICAgICAgICBwcm9ncmVzc2Jhci5zZXRQYXJlbnQoJChzZWxlY3RvcilbMF0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcHJvZ3Jlc3NiYXIuc3RhcnQoKTtcbiAgICAgICAgICAgIHJldHVybiBwcm9ncmVzc2JhcjtcbiAgICAgICAgfVxuICAgIH1cbn0pKCk7IiwiJ3VzZSBzdHJpY3QnO1xuXG5mdW5jdGlvbiBpc1VuZGVmaW5lZChkYXRhKSB7XG4gICAgcmV0dXJuIHR5cGVvZiBkYXRhID09PSAndW5kZWZpbmVkJztcbn1cblxuZnVuY3Rpb24gZ2V0Vmlldyhtb2R1bGVOYW1lLCB2aWV3TmFtZSkge1xuICAgIGlmIChpc1VuZGVmaW5lZCh2aWV3TmFtZSkpIHtcbiAgICAgICAgdmlld05hbWUgPSBtb2R1bGVOYW1lO1xuICAgIH1cbiAgICByZXR1cm4gJy4vdmlld3MvbW9kdWxlcy8nICsgbW9kdWxlTmFtZSArICcvJyArIHZpZXdOYW1lICsgJy5odG1sP3Y9JyArIHdpbmRvdy5odG1sX3ZlcnNpb247XG59XG5cbmZ1bmN0aW9uIGdldE5lc3RlZFZpZXcobW9kdWxlTmFtZSwgdmlld05hbWUpIHtcbiAgICBpZiAoaXNVbmRlZmluZWQodmlld05hbWUpKSB7XG4gICAgICAgIHZpZXdOYW1lID0gbW9kdWxlTmFtZTtcbiAgICB9XG5cbiAgICByZXR1cm4gJy4vdmlld3MvbW9kdWxlcy8nICsgbW9kdWxlTmFtZSArICcvJyArIHZpZXdOYW1lICsgJy8nICsgdmlld05hbWUgKyAnLmh0bWw/dj0nICsgd2luZG93Lmh0bWxfdmVyc2lvbjtcbn1cblxuZnVuY3Rpb24gZ2V0Q29tbW9uVmlldyhtb2R1bGVOYW1lLCB2aWV3TmFtZSkge1xuICAgIGlmIChpc1VuZGVmaW5lZCh2aWV3TmFtZSkpIHtcbiAgICAgICAgdmlld05hbWUgPSBtb2R1bGVOYW1lO1xuICAgIH1cblxuICAgIHJldHVybiAnLi92aWV3cy9jb21tb24vJyArIG1vZHVsZU5hbWUgKyAnLycgKyB2aWV3TmFtZSArICcuaHRtbD92PScgKyB3aW5kb3cuaHRtbF92ZXJzaW9uO1xufSIsIid1c2Ugc3RyaWN0JztcblxuKGZ1bmN0aW9uICgpIHtcbiAgICAndXNlIHN0cmljdCc7XG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLmNvbnRyb2xsZXJzJykuY29udHJvbGxlcignQ29uZmlybWF0aW9uTW9kYWxDdHJsJywgQ29uZmlybWF0aW9uTW9kYWxDdHJsKTtcblxuICAgIC8qIEBuZ0luamVjdCAqL1xuICAgIGZ1bmN0aW9uIENvbmZpcm1hdGlvbk1vZGFsQ3RybCgkc2NvcGUsIHBhc3NlZF9kYXRhLCAkdWliTW9kYWxJbnN0YW5jZSkge1xuXG4gICAgICAgIHZhciB2bSA9IHRoaXM7XG4gICAgICAgIHZtLm1lc3NhZ2UgPSBwYXNzZWRfZGF0YS5tZXNzYWdlO1xuICAgICAgICB2bS50aXRsZSA9IHBhc3NlZF9kYXRhLnRpdGxlO1xuICAgICAgICB2bS5zaG93X2NhbmNlbCA9IGZhbHNlO1xuICAgICAgICB2bS5jb25maXJtU2VsZWN0aW9uID0gY29uZmlybVNlbGVjdGlvbjtcbiAgICAgICAgdm0ucmVqZWN0U2VsZWN0aW9uID0gcmVqZWN0U2VsZWN0aW9uO1xuICAgICAgICB2bS5jYW5jZWwgPSBjYW5jZWw7XG4gICAgICAgIGFjdGl2YXRlKCk7XG5cbiAgICAgICAgZnVuY3Rpb24gYWN0aXZhdGUoKSB7XG4gICAgICAgICAgICBpZiAoYW5ndWxhci5pc0RlZmluZWQocGFzc2VkX2RhdGEueWVzX3RleHQpKSB7XG4gICAgICAgICAgICAgICAgdm0ueWVzX3RleHQgPSBwYXNzZWRfZGF0YS55ZXNfdGV4dDtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdm0ueWVzX3RleHQgPSBcIllFU1wiO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGFuZ3VsYXIuaXNEZWZpbmVkKHBhc3NlZF9kYXRhLm5vX3RleHQpKSB7XG4gICAgICAgICAgICAgICAgdm0ubm9fdGV4dCA9IHBhc3NlZF9kYXRhLm5vX3RleHQ7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHZtLm5vX3RleHQgPSBcIk5PXCI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoYW5ndWxhci5pc0RlZmluZWQocGFzc2VkX2RhdGEuY2FuY2VsX3RleHQpKSB7XG4gICAgICAgICAgICAgICAgdm0uc2hvd19jYW5jZWwgPSB0cnVlO1xuICAgICAgICAgICAgICAgIHZtLmNhbmNlbF90ZXh0ID0gcGFzc2VkX2RhdGEuY2FuY2VsX3RleHQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBjb25maXJtU2VsZWN0aW9uKCkge1xuICAgICAgICAgICAgJHVpYk1vZGFsSW5zdGFuY2UuY2xvc2Uoe1xuICAgICAgICAgICAgICAgIHN0YXR1czogJ3llcydcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIGZ1bmN0aW9uIHJlamVjdFNlbGVjdGlvbigpIHtcbiAgICAgICAgICAgICR1aWJNb2RhbEluc3RhbmNlLmNsb3NlKHtcbiAgICAgICAgICAgICAgICBzdGF0dXM6ICdubydcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gY2FuY2VsKCkge1xuICAgICAgICAgICAgJHVpYk1vZGFsSW5zdGFuY2UuZGlzbWlzcyh7XG4gICAgICAgICAgICAgICAgc3RhdHVzOiAnY2FuY2VsJ1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG59KSgpOyIsIid1c2Ugc3RyaWN0JztcblxuKGZ1bmN0aW9uICgpIHtcbiAgICAndXNlIHN0cmljdCc7XG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLmNvbnRyb2xsZXJzJykuY29udHJvbGxlcignSG9tZUN0cmwnLCBIb21lQ3RybCk7XG5cbiAgICAvKiBAbmdJbmplY3QgKi9cbiAgICBmdW5jdGlvbiBIb21lQ3RybCgkc3RhdGUsICR3aW5kb3csIFNJVEVfQ09OU1RBTlRTLCBQcm9ncmVzc0JhclNlcnZpY2UpIHtcbiAgICAgICAgdmFyIHZtID0gdGhpcztcbiAgICAgICAgYWN0aXZhdGUoKTtcblxuICAgICAgICBmdW5jdGlvbiBhY3RpdmF0ZSgpIHtcbiAgICAgICAgICAgIHZhciBwcm9ncmVzc19iYXIgPSBQcm9ncmVzc0JhclNlcnZpY2UuY3JlYXRlKCk7XG4gICAgICAgICAgICBwcm9ncmVzc19iYXIuY29tcGxldGUoKTtcbiAgICAgICAgfVxuICAgIH1cbn0pKCk7IiwiJ3VzZSBzdHJpY3QnO1xuXG4oZnVuY3Rpb24gKCkge1xuICAgICd1c2Ugc3RyaWN0JztcblxuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuY29udHJvbGxlcnMnKS5jb250cm9sbGVyKCdMb2dpbkN0cmwnLCBMb2dpbkN0cmwpO1xuXG4gICAgLyogQG5nSW5qZWN0ICovXG4gICAgZnVuY3Rpb24gTG9naW5DdHJsKCRzdGF0ZSwgJHdpbmRvdywgU0lURV9DT05TVEFOVFMsIFByb2dyZXNzQmFyU2VydmljZSkge1xuICAgICAgICB2YXIgdm0gPSB0aGlzO1xuICAgICAgICBhY3RpdmF0ZSgpO1xuXG4gICAgICAgIGZ1bmN0aW9uIGFjdGl2YXRlKCkge1xuICAgICAgICAgICAgdmFyIHByb2dyZXNzX2JhciA9IFByb2dyZXNzQmFyU2VydmljZS5jcmVhdGUoKTtcbiAgICAgICAgICAgIHByb2dyZXNzX2Jhci5jb21wbGV0ZSgpO1xuICAgICAgICB9XG4gICAgfVxufSkoKTsiLCIndXNlIHN0cmljdCc7XG5cbihmdW5jdGlvbiAoKSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb250cm9sbGVycycpLmNvbnRyb2xsZXIoJ1JlZ2lzdGVyQ3RybCcsIFJlZ2lzdGVyQ3RybCk7XG5cbiAgICAvKiBAbmdJbmplY3QgKi9cbiAgICBmdW5jdGlvbiBSZWdpc3RlckN0cmwoJHN0YXRlLCAkd2luZG93LCBTSVRFX0NPTlNUQU5UUywgUHJvZ3Jlc3NCYXJTZXJ2aWNlKSB7XG4gICAgICAgIHZhciB2bSA9IHRoaXM7XG4gICAgICAgIGFjdGl2YXRlKCk7XG5cbiAgICAgICAgZnVuY3Rpb24gYWN0aXZhdGUoKSB7XG4gICAgICAgICAgICB2YXIgcHJvZ3Jlc3NfYmFyID0gUHJvZ3Jlc3NCYXJTZXJ2aWNlLmNyZWF0ZSgpO1xuICAgICAgICAgICAgcHJvZ3Jlc3NfYmFyLmNvbXBsZXRlKCk7XG4gICAgICAgIH1cbiAgICB9XG59KSgpOyJdfQ==
