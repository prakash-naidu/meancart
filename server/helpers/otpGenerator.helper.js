module.exports = (function(){
    return function OtpGenerator(){
        this.generate = generate;
        this.generateOtpExp = generateOtpExp;

    };

    function generate(len){
        let length = len || 4;
        let otp = 1234;
        switch(length){
            case 1:{
                otp = Math.floor((9 * Math.random()) + 1);
                break;
            }
            case 2:{
                otp = Math.floor((99 * Math.random()) + 10);
                break;
            }
            case 3:{
                otp = Math.floor((999 * Math.random()) + 100);
                break;
            }
            case 4:{
                otp = Math.floor((9999 * Math.random()) + 1000);
                break;
            }
            case 5:{
                otp = Math.floor((99999 * Math.random()) + 10000);
                break;
            }
            case 6:{
                otp = Math.floor((999999 * Math.random()) + 100000);
                break;
            }
            case 7:{
                otp = Math.floor((9999999 * Math.random()) + 1000000);
                break;
            }
            case 8:{
                otp = Math.floor((99999999 * Math.random()) + 10000000);
                break;
            }
            case 9:{
                otp = Math.floor((999999999 * Math.random()) + 100000000);
                break;
            }
            case 10:{
                otp = Math.floor((9999999999 * Math.random()) + 1000000000);
                break;
            }
            default:{
                otp = Math.floor((9999 * Math.random()) + 1000);
                break;
            }
        }

        let outputLen = otp.toString().length;
        if(length === outputLen){
            return otp;
        }
        let tmpStr = otp.toString().substring(0,length);
        return parseInt(tmpStr);
    }
    function generateOtpExp(minutes) {
        let min = minutes || 5;
        return new Date(new Date().getTime() + (min * 60 * 1000)).toJSON()
    }
})();