module.exports = (function(){
    return {
        send:function(req,res,code,data){
            res.status(code).json(data);
        }
    }
})();