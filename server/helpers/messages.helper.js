module.exports = (function(){
    
    return new Messages();
    
    function Messages(){
        /*
        *   Error Messages
        *********************************************************************************
        *   All error messages should map here.
        */
        this.ERROR_400 = "something went wrong";
        this.ERROR_PASSWORD_MISMATCH = "password mismatch";
        this.ERROR_WRONG_PASSWORD = "Invalid password";
        this.ERROR_MIN_PASSWORD_LENGTH = "password must meet minimum length requirement";
        this.ERROR_USER_ALREADY_EXIST = "The specified account already exists.";
        this.ERROR_USER_NOT_FOUND = "requested user not found";
        this.ERROR_REQUIRED_FIELDS = "required fields are missing";
        this.ERROR_WRONG_OTP = "wrong otp";
        this.ERROR_EXPIRED_OTP = "expired otp";
    
        /*
        *   Success Messages
        *********************************************************************************
        *   All success messages should map here.
        */
        this.SUCCESS_SIGN_UP = "check email for otp";
        this.SUCCESS_RESEND_OTP = "check email for otp";
        this.SUCCESS_FORGOT_PASSWORD = "password reset successfully";
        this.SUCCESS_OTP_VERIFY = "otp verified successfully";
        this.SUCCESS_USER_LOGIN = "user login successfully";
    }
    
})();