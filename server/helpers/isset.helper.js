let _ = require('lodash');

module.exports = (function(){
    return function isset(obj) {
        let flag = true;
        if (_.isEmpty(obj)) {
            return false;
        }
        if (!_.isObject(obj)) {
            return !_.isUndefined(obj);
        }
        _.each(obj, function (val, key) {
            if (_.isUndefined(val)) {
                flag = false;
            }
        });
        return flag;
    };
})();