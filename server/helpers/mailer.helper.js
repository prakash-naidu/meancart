const nodemailer = require('nodemailer');
let Promise = require('bluebird');

module.exports = (function(){
    let smtpTransport = nodemailer.createTransport({
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        secure: process.env.MAIL_SECURE,
        auth: {
            user: process.env.MAIL_USERNAME,
            pass: process.env.MAIL_PASSWORD
        }
    });

    return function Mailer(reciever, subject, htmlBody){
        this.reciever = reciever || '';
        this.subject = subject || '';
        this.htmlBody = htmlBody || '';

        this.send = send;
    };

    function send() {
        let mailOptions = {
            to: this.reciever,
            subject: this.subject,
            html: this.htmlBody,
        };
        return smtpTransport.sendMail(mailOptions);
    }
})();