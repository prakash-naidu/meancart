require('dotenv').config();
let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let passport = require('passport');
let flash = require('connect-flash');

let routes = require('./routes/routes');

let app = express();

require('./config/db.config');
require('./config/passport.config');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.set('publicPath',path.join(__dirname, '/public'));

//uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(flash());
app.use(passport.initialize());
app.use(express.static(path.join(__dirname, 'public')));

/*
*   Custom Middleware
*********************************************************************************
*   custom middleware should use here
*/


routes.initialize(app);

module.exports = app;
