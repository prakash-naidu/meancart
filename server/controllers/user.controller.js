let UserService = require('../services/user.service');
let Response = require('../helpers/response.helper');
let messages = require('../helpers/messages.helper');


module.exports = (function(){
    let userService = new UserService();

    return new UserController();

    function UserController(){
        this.signUp = signUp;
        this.verifyOtp = verifyOtp;
        this.resendOtp = resendOtp;
        this.forgotPassword = forgotPassword;
        this.signIn = signIn;
    }

    function signUp(req, res, next){
        
        let req_data = {
            email:req.body.email,
            password:req.body.password,
            conf_password:req.body.conf_password
        };
        
        userService.signUp(req_data)
            .then((user)=>{
                let data ={error:false, message:messages.SUCCESS_SIGN_UP, user:{id:user._id}};
                Response.send(req, res, 200, data);
            })
            .catch((err)=>{
                if (err.code){
                    Response.send(req,res, err.code, err.data);
                }else {
                    Response.send(req, res, 400, {error: true, message: messages.ERROR_400});
                }
            });
        
    }

    function verifyOtp(req, res, next) {
        
        let req_data = {
            otp:req.body.otp,
            user_id:req.body.id,
        };
        
        userService.verifyOtp(req_data)
            .then((result)=>{
                Response.send(req, res, 200, {error:false,message:messages.SUCCESS_OTP_VERIFY, token:result.token});
            })
            .catch((err)=>{
                if (err.code){
                    Response.send(req, res, err.code, err.data);
                }else {
                    Response.send(req, res, 400, {error: true, message: messages.ERROR_400});
                }
            });

    }

    function resendOtp(req, res, next){
        
        let req_data = {
            email:req.body.email
        };
        
        userService.resendOtp(req_data)
            .then((user)=>{
                let data ={error:false, message:messages.SUCCESS_RESEND_OTP, user:{id:user._id}};
                Response.send(req, res, 200, data);
            })
            .catch((err)=>{
                if (err.code){
                    Response.send(req, res, err.code, err.data);
                }else {
                    Response.send(req, res, 400, {error: true, message: messages.ERROR_400});
                }
            })
        
    }

    function forgotPassword(req, res, next) {
        
        let req_data = {
            id:req.body.id,
            otp:req.body.otp,
            password:req.body.password,
            conf_password:req.body.conf_password
        };
        
        userService.forgotPassword(req_data)
            .then((user)=>{
                Response.send(req, res, 200, {error:false, message:messages.SUCCESS_FORGOT_PASSWORD});
            })
            .catch((err)=>{
                if (err.code){
                    Response.send(req, res, err.code, err.data);
                }else {
                    Response.send(req, res, 400, {error: true, message: messages.ERROR_400});
                }
            });
        
    }
    
    function signIn(req, res, next) {
        let req_data = {
            email:req.body.email,
            password:req.body.password
        };
        
        userService.signIn(req_data)
            .then((result)=>{
                if (result.token){
                    let data = {error:false, message:messages.SUCCESS_USER_LOGIN,token:result.token};
                    Response.send(req, res, 200, data)
                }
                else{
                    let data ={error:false, message:messages.SUCCESS_RESEND_OTP, user:{id:result._id}};
                    Response.send(req, res, 200, data);
                }
            })
            .catch((err)=>{
                console.log(err);
                if (err.code){
                    Response.send(req, res, err.code, err.data);
                }else {
                    Response.send(req, res, 400, {error: true, message: messages.ERROR_400});
                }
            })
        
    }

})();