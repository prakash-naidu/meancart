let Promise = require("bluebird");
let bcrypt = require('bcrypt-nodejs');

let UserRepository = require('../repositories/user.repository');
let OTPGenerator = require('../helpers/otpGenerator.helper');
let Mailer = require('../helpers/mailer.helper');
let isset = require('../helpers/isset.helper');
let messages = require('../helpers/messages.helper');

module.exports = (function(){
    
    let userRepository = new UserRepository();
    let otpGenerator = new OTPGenerator();

    return function UserService(){

        this.signUp = signup;
        this.generateOtpAndSendEmail = generateOtpAndSendEmail;
        this.verifyOtp = verifyOtp;
        this.encryptPassword = encryptPassword;
        this.generateJwt = generateJwt;
        this.comparePassword = comparePassword;
        this.resendOtp = resendOtp;
        this.forgotPassword = forgotPassword;
        this.signIn = signIn;

    };


    function signup(data){
        if (isset(data)){
            if(data.password !== data.conf_password){
                 return new Promise.reject({code:400,data:{error:true,message:messages.ERROR_PASSWORD_MISMATCH}});
            }

            return userRepository.getUserByEmail(data.email)
                .then((user)=>{
                    if (user){
                        return new Promise.reject({code:409,data:{error:true, message:messages.ERROR_USER_ALREADY_EXIST}});
                    }
                    let encrypted_pass = this.encryptPassword(data.password);
                    user = {
                        email:data.email,
                        password:encrypted_pass
                    };
                    return this.generateOtpAndSendEmail(user);
                })
                .then((user)=>{
                    user.status = "P";
                    user.otp_exp = otpGenerator.generateOtpExp(10);// passed argument in minutes
                    return userRepository.saveUser(user);
                })
                .catch((err)=>{
                    return new Promise.reject(err);
                });
        }else{
            return new Promise.reject({code:422,data:{error:true,message:messages.ERROR_REQUIRED_FIELDS}});
        }
    }

    function generateOtpAndSendEmail(user){
        let otp = otpGenerator.generate(4);
        let mailer = new Mailer(user.email, "Verify Otp", otp + " is your one time password.");
        return mailer.send()
            .then(function(info){
                user.otp = otp;
                return new Promise.resolve(user);
            })
            .catch(function(err){
                return new Promise.reject(err);
            });
    }

    function verifyOtp(data){
        if(isset(data)){
            return userRepository.getUserByID(data.user_id)
                .then((user)=>{
                    if(data.otp !== user.otp){
                        return new Promise.reject({code:400,data:{error:true,message:messages.ERROR_WRONG_OTP}});
                    }
                    if(new Date(user.otp_exp) <= new Date()){
                        return new Promise.reject({code:400,data:{error:true,message:messages.ERROR_EXPIRED_OTP}});
                    }
                    return this.generateJwt(user);
                })
                .then((result)=>{
                    return userRepository.updateUser({_id:result._id},{token:result.token,status:"V",otp:null, otp_exp:null});
                })
                .catch((err)=>{
                    return new Promise.reject(err);
                });
        }else{
            return new Promise.reject({code:422,data:{error:true,message:messages.ERROR_REQUIRED_FIELDS}});
        }
    }

    function encryptPassword(password){
        return bcrypt.hashSync(password, bcrypt.genSaltSync(5), null);
    }

    function comparePassword(password,encryptedPassword){
        return bcrypt.compareSync(password, encryptedPassword);
    }

    function generateJwt(payload){
        return new Promise.resolve({_id:payload._id,token:"token"});
    }

    function resendOtp(data){
        if(isset(data)){
            return userRepository.getUserByEmail(data.email)
                .then((user)=>{
                    if (user){
                        return this.generateOtpAndSendEmail(user);
                    }
                    return new Promise.reject({code:400,data:{error:true,message:messages.ERROR_USER_NOT_FOUND}});
                })
                .then((user)=>{
                    let condition = {_id:user._id};
                    let data = {otp:user.otp, otp_exp:otpGenerator.generateOtpExp(10)};
                    return userRepository.updateUser(condition,data);
                })
                .catch((err)=>{
                    return new Promise.reject(err);
                })
        }else{
            return new Promise.reject({code:422,data:{error:true,message:messages.ERROR_REQUIRED_FIELDS}});
        }
    }
    
    function forgotPassword(data) {
        if (isset(data)){
            if (data.password.length < 8){
                return new Promise.reject({code:422,data:{error:true,message:messages.ERROR_MIN_PASSWORD_LENGTH}});
            }
            
            if(data.password !== data.conf_password){
                return new Promise.reject({code:422,data:{error:true,message:messages.ERROR_PASSWORD_MISMATCH}});
            }
            
            return userRepository.getUserByID(data.id)
                .then((user)=>{
                    if(data.otp !== user.otp){
                        return new Promise.reject({code:400,data:{error:true,message:messages.ERROR_WRONG_OTP}});
                    }
                    if(new Date(user.otp_exp) <= new Date()){
                        return new Promise.reject({code:400,data:{error:true,message:messages.ERROR_EXPIRED_OTP}});
                    }
                    return userRepository.updateUser({_id:user._id},{password:this.encryptPassword(user.password)});
                })
                .catch((err)=>{
                    return new Promise.reject(err);
                })
        }else{
            return new Promise.reject({code:422,data:{error:true,message:messages.ERROR_REQUIRED_FIELDS}});
        }
    }
    
    function signIn(data) {
        if (isset(data)){
            return userRepository.getUserByEmail(data.email)
                .then((user)=>{
                    if (user){
                        if (this.comparePassword(data.password,user.password)){
                            return user;
                        }
                        return new Promise.reject({code:400,data:{error:true,message:messages.ERROR_WRONG_PASSWORD}});
                    }
                    return new Promise.reject({code:400,data:{error:true,message:messages.ERROR_USER_NOT_FOUND}});
                    
                })
                .then((user)=>{
                    if (user.status === "P"){
                        return generateOtpAndSendEmail(user)
                    }
                    return this.generateJwt(user);
                })
                .then((result)=>{
                    if (result.otp){
                        let condition = {_id:result._id};
                        let data = {otp:result.otp, otp_exp:otpGenerator.generateOtpExp(10)};
                        return userRepository.updateUser(condition,data);
                    }
                    return new Promise.resolve(result)
                })
                .catch((err)=>{
                    return new Promise.reject(err);
                })
        }else{
            return new Promise.reject({code:422,data:{error:true,message:messages.ERROR_REQUIRED_FIELDS}});
        }
    }
})();