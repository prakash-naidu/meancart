var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var watch = require('gulp-watch');
var autoprefixer = require('gulp-autoprefixer');
var babel = require('gulp-babel');
var shell = require('gulp-shell');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var del = require('del');
var cleanCSS = require('gulp-clean-css');
var gulpsync = require('gulp-sync')(gulp);
var env = require('gulp-env');
var gulpif = require('gulp-if');

var nm = './node_modules/';
var bw = './bower_components/';
var app_external = "./app/external/";
var app = "./client/app/";
var pub = "./server/public/"

env({
    file: 'frontend.env.json'
});

console.log(process.env.environment);

gulp.task('sass', function () {
    gulp
    .src(app + 'scss/app.scss')
    .pipe(gulpif(process.env.environment === 'local', sourcemaps.init()))
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(plumber())
    .pipe(concat('app.css'))
    .pipe(cleanCSS())
    .pipe(gulpif(process.env.environment === 'local', sourcemaps.write()))
    .pipe(gulp.dest( pub + 'css/'))
});

gulp.task('vendorjs', function () {

    gulp.src([
        nm + 'jquery/dist/jquery.min.js',
        nm + 'bootstrap-sass/assets/javascripts/bootstrap.min.js',

        nm + 'angular/angular.min.js',
        nm + 'angular-sanitize/angular-sanitize.min.js',
        nm + 'angular-ui-router/release/angular-ui-router.min.js',
        nm + 'angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
        nm + 'ramda/dist/ramda.min.js',
        nm + 'angular-validation/dist/angular-validation.min.js',
        app_external + 'angular-validation-rule.js',
        bw + 'ngprogress/build/ngProgress.min.js',
        nm + 'toastr/build/toastr.min.js',
        nm + 'angular-permission/dist/angular-permission.min.js',
        nm + 'angular-permission/dist/angular-permission-ui.min.js',
        nm + 'angular-permission/dist/angular-permission-ng.min.js',
    ])
    .pipe(plumber())
    .pipe(concat('vendors.js'))
    .pipe(gulp.dest(pub + 'js/'));

});

gulp.task('appjs', function () {

    gulp.src([
        app + 'index.js',
        app + 'app.config.js',
        app + "**/*.js",
    ])
    .pipe(babel({ presets: ['es2015'] }))
    .pipe(plumber())
    .pipe(gulpif(process.env.environment === 'local', sourcemaps.init()))
    .pipe(concat('app.js'))
    .pipe(ngAnnotate({add: true}))
    .pipe(gulpif(process.env.environment === 'local', sourcemaps.write()))
    .pipe(gulp.dest( pub + 'js/'));

});
gulp.task('copyhtml', function () {
    gulp.src([app + '**/*.html', '!'+app + 'index.html']).pipe(gulp.dest( pub +'views'));
});
gulp.task('copyconfig', function () {
    gulp
    .src(['frontend.constants.js'])
    .pipe(concat('config.js'))
    .pipe(gulp.dest( pub + 'js'));
});

gulp.task('cleanall', function () {

    del([
        pub+'css',
        pub+'js',
        pub+'views/*',
        pub+'index.html'
    ]);
});

gulp.task('makeindex', function () {
    del(pub + 'views/index.html');
    gulp.src([app + 'index.html']).pipe(gulp.dest(pub));
});

gulp.task('watch', function () {
    gulp.watch(app + '**/*.scss', ['sass']);
    gulp.watch(app + '**/*.js', ['appjs']);
    gulp.watch(app + '**/*.html', ['copyhtml']);
});

gulp.task('default', gulpsync.sync(['cleanall', 'makeindex', ['sass', 'vendorjs', 'appjs', 'copyconfig', 'copyhtml']]));

